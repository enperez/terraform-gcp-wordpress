# vars definitions

# region
region = "europe-west2"

# availability zone
zone = "europe-west2-a"

# project
project = "gcp-training-1-273301"

# instance name
name = "bitnami-gcp-vm"

# vm size
vm_type = "f1-micro"

# the iso
imagedisk = "coreos-stable-2345-3-0-v20200302"
